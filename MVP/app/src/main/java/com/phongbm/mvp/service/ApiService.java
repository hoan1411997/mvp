package com.phongbm.mvp.service;

import com.phongbm.mvp.model.IpAddress;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    // API URL: https://www.trackip.net/ip?json
    // @GET(value = "/ip")
    @GET("/ip?json")
    Call<IpAddress> getIP();

}