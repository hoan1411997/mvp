package com.phongbm.mvp.base;

public interface BaseView<T extends BasePresenter> {
    T getPresenter();

    T createPresenter();

}