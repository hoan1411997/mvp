package com.phongbm.mvp.model;

import com.google.gson.annotations.SerializedName;

public class IpAddress {
    @SerializedName("Country")
    private String country;

    @SerializedName("IP")
    private String ip;

    @SerializedName("Time")
    private String time;

    @SerializedName("UA")
    private String ua;

    public IpAddress() {
    }

    @Override
    public String toString() {
        return "IpAddress{" +
                "country='" + country + '\'' +
                ", ip='" + ip + '\'' +
                ", time='" + time + '\'' +
                ", ua='" + ua + '\'' +
                '}';
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

}