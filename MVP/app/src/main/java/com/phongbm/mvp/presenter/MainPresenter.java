package com.phongbm.mvp.presenter;

import com.phongbm.mvp.base.BasePresenter;

public interface MainPresenter extends BasePresenter {
    void signIn(String email, String password);

    void getIpAddress();

}