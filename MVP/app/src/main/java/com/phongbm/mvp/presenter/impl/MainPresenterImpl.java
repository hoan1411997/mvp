package com.phongbm.mvp.presenter.impl;

import android.support.annotation.NonNull;

import com.phongbm.mvp.base.BasePresenterImpl;
import com.phongbm.mvp.model.IpAddress;
import com.phongbm.mvp.presenter.MainPresenter;
import com.phongbm.mvp.service.ApiServiceBuilder;
import com.phongbm.mvp.view.activity.MainView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenterImpl extends BasePresenterImpl<MainView> implements MainPresenter {
    public MainPresenterImpl(MainView view) {
        super(view);
    }

    @Override
    public void signIn(String email, String password) {
        if (email == null || email.isEmpty()
                || password == null || password.isEmpty()){
            getView().signInFailed();
            return;
        }
        if (email.length() < 8 && password.length() < 6) {
            getView().signInFailed();
            return;
        }
        getView().signInSuccessful();
    }

    @Override
    public void getIpAddress() {
        ApiServiceBuilder.getService().getIP().enqueue(new Callback<IpAddress>() {
            @Override
            public void onResponse(@NonNull Call<IpAddress> call, @NonNull Response<IpAddress> response) {
                IpAddress ipAddress = response.body();
                if (ipAddress != null) {
                    getView().showSuccessMessage(ipAddress.toString());
                } else {
                    getView().showFailedMessage("Some error!");
                }
            }

            @Override
            public void onFailure(@NonNull Call<IpAddress> call, @NonNull Throwable t) {
                t.printStackTrace();
                getView().showFailedMessage(t.getMessage());
            }
        });
    }

}