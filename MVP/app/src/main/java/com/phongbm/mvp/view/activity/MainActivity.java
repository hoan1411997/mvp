package com.phongbm.mvp.view.activity;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.phongbm.mvp.R;
import com.phongbm.mvp.base.BaseActivity;
import com.phongbm.mvp.presenter.MainPresenter;
import com.phongbm.mvp.presenter.impl.MainPresenterImpl;

public class MainActivity
        extends BaseActivity<MainPresenter>
        implements MainView, View.OnClickListener {
    private Button btnSignIn;

    @Override
    public MainPresenter createPresenter() {
        return new MainPresenterImpl(this);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public void initializeComponents() {
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
                signIn();
                break;

            default:
                break;
        }
    }

    private void signIn() {
        // String email = "mail@gmail.com";
        // String password = "a@123456";
        // getPresenter().signIn(email, password);
        getPresenter().getIpAddress();
    }

    @Override
    public void signInSuccessful() {
        // TODO
    }

    @Override
    public void signInFailed() {
        // TODO
    }

    @Override
    public void showSuccessMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFailedMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

}