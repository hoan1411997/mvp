package com.phongbm.mvp.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiServiceBuilder {
    private static final String BASE_URL = "https://www.trackip.net";
    private static ApiServiceBuilder instance;

    public static ApiService getService() {
        if (instance == null) {
            instance = new ApiServiceBuilder();
        }
        return instance.apiService;
    }

    private ApiService apiService;

    private ApiServiceBuilder() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(ApiService.class);
    }

}