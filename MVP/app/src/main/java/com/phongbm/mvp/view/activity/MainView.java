package com.phongbm.mvp.view.activity;

import com.phongbm.mvp.base.BaseView;
import com.phongbm.mvp.presenter.MainPresenter;

public interface MainView extends BaseView<MainPresenter> {
    void signInSuccessful();

    void signInFailed();

    void showSuccessMessage(String msg);

    void showFailedMessage(String msg);

}